libpdl-ccs-perl (1.23.20-3) UNRELEASED; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.6.2, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 18 Jan 2023 17:23:41 +0100

libpdl-ccs-perl (1.23.20-2) unstable; urgency=medium

  * Team upload.
  * Don't remove files in debian/clean.
  * Bump Standards-Version to 4.6.1, no changes.
  * Add Rules-Requires-Root to control file.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 02 Dec 2022 08:54:31 +0100

libpdl-ccs-perl (1.23.20-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 20 Apr 2022 18:39:25 +0200

libpdl-ccs-perl (1.23.19-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 15 Apr 2022 06:59:14 +0200

libpdl-ccs-perl (1.23.18-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 18 Feb 2022 17:36:55 +0100

libpdl-ccs-perl (1.23.17-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with PDL 2.070.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 30 Jan 2022 11:44:26 +0100

libpdl-ccs-perl (1.23.17-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update copyright file.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 19 Jan 2022 08:55:57 +0100

libpdl-ccs-perl (1.23.16-3) unstable; urgency=medium

  * Team upload.
  * Rebuild with PDL 2.061.
  * Bump Standards-Version to 4.6.0, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 13 Nov 2021 12:12:20 +0100

libpdl-ccs-perl (1.23.16-2) unstable; urgency=medium

  * Team upload.
  * No-change rebuild with PDL 2.057.
    (closes: #993428)

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 05 Sep 2021 06:59:17 +0200

libpdl-ccs-perl (1.23.16-1) unstable; urgency=medium

  * Team upload.
  * Move from experimental to unstable.

 -- Bas Couwenberg <sebastic@debian.org>  Sun, 15 Aug 2021 19:22:34 +0200

libpdl-ccs-perl (1.23.16-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 30 Apr 2021 06:23:41 +0200

libpdl-ccs-perl (1.23.15-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Update copyright file.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 28 Apr 2021 06:25:58 +0200

libpdl-ccs-perl (1.23.14-1~exp1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.5.1, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Mon, 26 Apr 2021 17:41:26 +0200

libpdl-ccs-perl (1.23.13-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Drop spelling-errors.patch, applied upstream.

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 19 Nov 2020 09:26:12 +0100

libpdl-ccs-perl (1.23.12-2) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.5.0, no changes.
  * Add gbp.conf to use pristine-tar & --source-only changes by default.
  * Add patch to fix spelling errors.
  * Add lintian overrides for hardening-no-fortify-functions.

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 19 Nov 2020 06:00:51 +0100

libpdl-ccs-perl (1.23.12-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    Fixes "libpdl-ccs-perl FTBFS on architectures where char is unsigned"
    (Closes: #902192)
  * Declare compliance with Debian Policy 4.2.1.

 -- gregor herrmann <gregoa@debian.org>  Tue, 28 Aug 2018 16:42:47 +0200

libpdl-ccs-perl (1.23.10-1) unstable; urgency=medium

  * Team upload.

  [ Laurent Baillet ]
  * Bump Standards-Version to 4.2.0, no changes.

  [ gregor herrmann ]
  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Fri, 24 Aug 2018 16:08:03 +0200

libpdl-ccs-perl (1.23.9-1) unstable; urgency=medium

  * New upstream version 1.23.9

 -- Laurent Baillet <laurent.baillet@gmail.com>  Fri, 22 Jun 2018 14:18:37 +0000

libpdl-ccs-perl (1.23.8-1) unstable; urgency=low

  * Initial release (Closes: #891885).

 -- Laurent Baillet <laurent.baillet@gmail.com>  Tue, 19 Jun 2018 14:57:33 +0000
